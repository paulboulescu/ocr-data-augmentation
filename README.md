# OCR Data Augmentation

## About
OCR data augmentation tool, built using OpenCV, Pillow, and Scikit-Image.

![Result](resources/result.png)

## Description
New images are generated in two steps. First, wrinkles with different intensities are randomly generated across the image. Secondly, perspective transformation is added. All coordinates of the labeled elements from the original image are recalculated and saved.

## Installation

1. Clone the repository
```
$ git clone https://paulboulescu@bitbucket.org/paulboulescu/ocr-data-augmentation.git
```

2. Create only one augmentation for each of the input images 
```
$ python ocr_data_augmentation.py
```

3. Create multiple augmentations for each of the input images
```
$ python ocr_data_augmentation.py -number <number of augmentations>
```

4. Display each generated image next to its original, as well as all the labeled elements from each of the two versions
```
$ python ocr_data_augmentation.py --plot
```

![Labeled Elements](resources/labels.png)

## Requirements
1. OpenCV
2. Pillow
3. Scikit-Image

## Details
* All input images have to be placed inside the `input/` folder
* Augmented images will be generated inside the `output/` folder
* `input/data.json` stores information regarding images and their labeled boxes
* `output/data.json` will be created and will contain information regarding the generated images and their labeled elements
