
from utils import *
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from skimage.transform import PiecewiseAffineTransform, warp
from utils import *
import cv2

class ImageAugmentation:

    def __init__(self, path, boxes):
        self.resources = self.get_resources(path)
        self.resources['input_boxes'] = boxes
        self.resources['wrinkled_boxes'] = []
        self.resources['perspective_boxes'] = []

    def get_resources(self, path):

        resources = {}

        # load the image
        resources['input_image'] = np.array(Image.open('input/'+path), dtype=np.uint8)

        # add alpha channel
        resources['input_image'] = self.add_alpha_channel(resources['input_image'], 'RGB')

        # dimensions of the image
        resources['image_height'] = resources['input_image'].shape[0]
        resources['image_width'] = resources['input_image'].shape[1]

        # set the maximum area for a wrinkle, relative to the dimensions of the image
        resources['max_wrinkle_area'] =  np.maximum(resources['image_height'], resources['image_width'])/8

        # set the maximum distance for corner perspective, relative to the dimensions of the image
        resources['max_perspective_area'] = np.maximum(resources['image_height'], resources['image_width']) / 10

        return resources

    def add_alpha_channel(self, original_img, order):
        # checks if the image doesn't already have an alpha channel
        if np.shape(original_img)[2] == 3:
            # the PIL image case
            if order == 'RGB':
                r_channel, b_channel, g_channel = cv2.split(original_img)
                alpha_channel = np.ones(r_channel.shape, dtype=r_channel.dtype) * 255
                output_image = cv2.merge((r_channel, g_channel, b_channel, alpha_channel))
            # the CV2 image case
            elif order == 'BGR':
                b_channel, g_channel, r_channel = cv2.split(original_img)
                alpha_channel = np.ones(r_channel.shape, dtype=r_channel.dtype)
                output_image = cv2.merge((b_channel, g_channel, r_channel, alpha_channel))
            return output_image
        else:
            return original_img


    def add_wrinkles(self):

        resources = self.resources

        # set a safe margin
        safe_margin = 0

        min_anchors_no = 4
        max_anchors_no = 35

        # the number of anchors that will be placed across the image ( + another 10 around the edges)
        anchors_no = np.random.randint(min_anchors_no, max_anchors_no)

        # randomly selected anchors across the image
        orig_x_anchors = np.random.randint(safe_margin, resources['image_width'] - safe_margin, anchors_no)
        orig_y_anchors = np.random.randint(safe_margin, resources['image_height'] - safe_margin, anchors_no)

        # add the corners
        orig_x_anchors = np.append(orig_x_anchors, [0, resources['image_width'], resources['image_width'], 0])
        orig_y_anchors = np.append(orig_y_anchors, [0, 0, resources['image_height'], resources['image_height']])

        # add margin points
        orig_x_anchors = np.append(orig_x_anchors,
                                   [resources['image_width'] / 3, 2 * resources['image_width'] / 3,
                                    resources['image_width'], 2 * resources['image_width'] / 3,
                                    resources['image_width'] / 3, 0])
        orig_y_anchors = np.append(orig_y_anchors,
                                   [0, 0, resources['image_height'] / 2, resources['image_height'],
                                    resources['image_height'], resources['image_height'] / 2])

        # will hold the deviated positions of each anchor
        deviated_x_anchors = np.copy(orig_x_anchors).astype(float)
        deviated_y_anchors = np.copy(orig_y_anchors).astype(float)

        # random deviation on x and y for each anchor
        rand_deviations_x = np.random.rand(np.shape(deviated_x_anchors)[0])
        rand_deviations_y = np.random.rand(np.shape(deviated_y_anchors)[0])

        # list of anchors
        anchors = []
        # create anchors
        for anchor_index in range(len(orig_x_anchors)):
            anchor = {}
            anchor['original_x'] = orig_x_anchors[anchor_index]
            anchor['original_y'] = orig_y_anchors[anchor_index]
            anchor['diameter'] = 0
            anchors.append(anchor)

        # give anchors surface
        for anchor_index, anchor in enumerate(anchors):

            # loops over and fixes any radius dimensions until no anchors are overlapping
            invalid_area = True
            while invalid_area:
                invalid_area = False
                anchor['diameter'] = np.random.randint(0, resources['max_wrinkle_area'])

                for check_index in range(len(anchors)):

                    # skip the current anchor from checking
                    if anchor_index != check_index:
                        # calculate distance between the two anchors's centers
                        distance = np.sqrt(
                            np.square(anchor['original_x'] - anchors[check_index]['original_x']) + np.square(
                                anchor['original_y'] - anchors[check_index]['original_y']))

                        # if True, the two anchors are overlapping
                        if distance < (anchor['diameter'] + anchors[anchor_index]['diameter']):
                            invalid_area = True
                            # the area of the current anchor will be reduced to the maximum non-overlaping valing
                            anchor['diameter'] = distance - anchors[anchor_index]['diameter']

        # calculate the deviated anchors positions
        for anchor_index, anchor in enumerate(anchors):
            # determine the center of the image
            image_center_x = resources['image_width'] / 2
            image_center_y = resources['image_height'] / 2

            # orientates the wrinkle towards inside of the image
            orientation_x = ((image_center_x - anchor['original_x']) / image_center_x)
            orientation_y = ((image_center_y - anchor['original_y']) / image_center_y)

            # apply deviation to each anchor, using correct orientation, towards inside
            anchor['deviated_x'] = (
                    anchor['original_x'] - rand_deviations_x[anchor_index] * orientation_x * anchor['diameter'])
            anchor['deviated_y'] = (
                    anchor['original_y'] - rand_deviations_y[anchor_index] * orientation_y * anchor['diameter'])


        # create the transform points array
        transform_src = np.empty((len(anchors), 2))
        transform_dst = np.empty((len(anchors), 2))

        for anchor_index, anchor in enumerate(anchors):
            transform_src[anchor_index][0] = anchor['original_x']
            transform_src[anchor_index][1] = anchor['original_y']
            transform_dst[anchor_index][0] = anchor['deviated_x']
            transform_dst[anchor_index][1] = anchor['deviated_y']


        # create the PiecewiseAffineTransform used to produce the wrinkle effect
        wrinkles_tform = PiecewiseAffineTransform()

        # calculate transformation of the image
        wrinkles_tform.estimate(transform_src, transform_dst)

        # apply transformation to OCR label coordinates
        self.wrinkle_boxes(wrinkles_tform)

        # wrinkle the image
        wrinkles_image = warp(resources['input_image'], wrinkles_tform, output_shape=(resources['image_height'], resources['image_width']))

        # convert to CV2 image
        wrinkles_image = np.array(wrinkles_image)

        # convert RGB to BGR
        wrinkles_image = wrinkles_image[:, :, :].copy()

        self.resources['wrinkles_image'] = wrinkles_image


    def wrinkle_boxes(self, tform):
        for box_index, box in enumerate(self.resources['input_boxes']):
            self.resources['wrinkled_boxes'].append(tform.inverse(box))


    def perspective_boxes(self, perspective_matrix):
        for box_index, box in enumerate(self.resources['wrinkled_boxes']):
            [perspective_box] = np.array(cv2.perspectiveTransform(np.array([box], dtype=np.float32), perspective_matrix), dtype=np.int32)
            self.resources['perspective_boxes'].append(perspective_box)


    def add_perspective(self):

        resources = self.resources

        original_corners = np.float32([[0, 0], [0, resources['image_height']], [resources['image_width'], resources['image_height']], [resources['image_width'], 0]])

        # generates random values for each corner's deviation
        rand_corners_deviation_x = np.random.rand(4)
        rand_corners_deviation_y = np.random.rand(4)

        # calculate perspective corners position

        # start with the initial positions
        perspective_corners = np.copy(original_corners)

        # less perspective is applied on the shorter dimension
        x_perspective_coef = resources['image_width']/np.maximum(resources['image_width'], resources['image_height'])
        y_perspective_coef = resources['image_height'] / np.maximum(resources['image_width'], resources['image_height'])

        # top-left corner deviations (towards inside)
        perspective_corners[0][0] += resources['max_perspective_area'] * rand_corners_deviation_x[0] * x_perspective_coef
        perspective_corners[0][1] += resources['max_perspective_area'] * rand_corners_deviation_y[0] * y_perspective_coef

        # bottm-left corner deviations (towards inside)
        perspective_corners[1][0] += resources['max_perspective_area'] * rand_corners_deviation_x[1] * x_perspective_coef
        perspective_corners[1][1] -= resources['max_perspective_area'] * rand_corners_deviation_y[1] * y_perspective_coef

        # bottom-righr corner deviations (towards inside)
        perspective_corners[2][0] -= resources['max_perspective_area'] * rand_corners_deviation_x[2] * x_perspective_coef
        perspective_corners[2][1] -= resources['max_perspective_area'] * rand_corners_deviation_y[2] * y_perspective_coef

        # top-right corner deviations (towards inside)
        perspective_corners[3][0] -= resources['max_perspective_area'] * rand_corners_deviation_x[3] * x_perspective_coef
        perspective_corners[3][1] += resources['max_perspective_area'] * rand_corners_deviation_y[3] * y_perspective_coef

        # create the transform matrix
        perspective_matrix = cv2.getPerspectiveTransform(original_corners, perspective_corners)

        # calculate new coordinates for OCR label
        self.perspective_boxes(perspective_matrix)

        # apply perspective transform over the image
        perspective_image = cv2.warpPerspective(resources['wrinkles_image'], perspective_matrix, (resources['image_width'], resources['image_height']))

        self.resources['perspective_image'] = perspective_image


    def save_image(self, name):
        # save the output image file
        cv2.imwrite(name, self.resources['perspective_image'] * 255)


    def plot_image(self, ):

        resources = self.resources

        # initial image plot with boxes
        plt.figure(1)
        plt.subplot(211)
        # draw polygon of OCR label
        for box_index, box in enumerate(resources['input_boxes']):
            cv2.polylines(resources['input_image'], [box], True, (255, 0, 0, 255), 1)
        # display the input image
        plt.imshow(resources['input_image'])
        plt.axis('off')
        plt.title('Input')

        # output image plot with boxes
        plt.subplot(212)
        # draw polygon of perspective boxes
        for box_index, box in enumerate(resources['perspective_boxes']):
            cv2.polylines(resources['perspective_image'], [box], True, (1, 0, 0, 1), 1)

        # display output image
        plt.imshow(resources['perspective_image'])
        plt.axis('off')
        plt.title('Output')

        # show result
        plt.show()
