
import sys, getopt
from utils import *
from model import ImageAugmentation


def main(argv):
    plot = False
    aug_number = 1

    try:
        opts, args = getopt.getopt(argv,'hn:p',['number=','plot'])
    except getopt.GetoptError:
        print('ocr_data_augmentation.py [options...] \n--number\tNumber of outputs created for each file\n--plot\t\tShows each augmenatation')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print('ocr_data_augmentation.py [options...] \n--numbertNumber of outputs created for each file\n--plot\t\tShows each augmenatation')
            sys.exit()
        elif opt in ('-n', '--number'):
            aug_number = int(arg)
        elif opt in ('-p', '--plot'):
            plot = True

    # load labels
    input_images_data = load_labels()

    # used for naming output files
    output_counter = 0

    # used to store the output labels
    output_images_data = {}

    # loops over all the input images
    for image_index, image in enumerate(input_images_data):

        # loops over the augmented data of a single image
        for aug_index in range(aug_number):
            augmentation = ImageAugmentation(image['path'], image['box_coordinates'])
            augmentation.add_wrinkles()
            augmentation.add_perspective()
            augmentation.save_image('output/'+image['name']+'_output_'+str(aug_index) + '.png')

            # used to store output labels for one image
            output_image = {}
            output_image['path'] = image['name']+'_output_'+str(aug_index) + '.png'
            output_image['name'] = image['name']
            output_image['boxes'] = {}

            # loops over the boxes of one image
            for box_index, box in enumerate(augmentation.resources['perspective_boxes']):

                output_box = {}
                output_box['value'] = image['box_values'][box_index]
                output_box['coords'] = box.tolist()
                output_image['boxes']['box_' + str(box_index)] = output_box

            output_images_data['image_'+str(output_counter)] = output_image

            if plot:
                augmentation.plot_image()

            output_counter += 1

    # saves output labels
    save_labels(output_images_data)

if __name__ == '__main__':
   main(sys.argv[1:])
