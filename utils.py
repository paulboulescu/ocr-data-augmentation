
import numpy as np
import json


def load_labels():

    with open('input/data.json', 'r') as file:
        loaded_data = json.load(file)

    images = []

    for image_key, image_data in loaded_data.items():

        image = {}
        image['name'] = image_data['name']
        image['path'] = image_data['path']
        image['box_coordinates'] = []
        image['box_values'] = []

        for box_key, box_data in image_data['boxes'].items():

            image['box_values'].append(box_data['value'])

            box = np.array(box_data['coords'])
            image['box_coordinates'].append(box)

        images.append(image)

    return images


def save_labels(data):
    with open("output/data.json", mode='w+') as file:
        json.dump(data, file, indent=2)
